import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:login/views/login_page.dart';
import 'package:login/view_models/login_view_model.dart';
import 'package:homepage/views/home_page.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void main() => runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => LogInViewModel(),
          ),
        ],
        child: const MyApp(),
      ),
    );

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  Locale _locale = const Locale('en');

  void setLocale(Locale newLocale) {
    setState(() {
      _locale = newLocale;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SoftDebut',
      theme: ThemeData(primarySwatch: Colors.blue),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      locale: _locale,
      home: LoginPage(onLocaleChange: setLocale),
      routes: {
        '/login': (context) => LoginPage(onLocaleChange: setLocale),
        '/home': (context) => const HomePage(),
      },
    );
  }
}