class UserModel{
  //final String tenancyCode;
  final String username;
  final String email;
  final String password;

  UserModel({/*required this.tenancyCode,*/ required this.username,required this.email, required this.password});

  factory UserModel.fromJson(Map<String, dynamic> json){
    return UserModel(
      //tenancyCode: json['firstName'],
      username: json['username'],
      email: json['email'],
      password: json['password']
    );
  }
}