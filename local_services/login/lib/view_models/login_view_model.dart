import 'dart:convert';

import 'package:flutter/cupertino.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:login/models/user.dart';
import 'package:login/services/auth_service.dart';

class LogInViewModel with ChangeNotifier {
  final AuthService _authService;
  UserModel? _loggedInUser;
  bool _passwordVisible = false;
  bool _showValidationMessage = false;
  bool _isLoading = false;
  String? _error;

  String? tenancyCodeError;
  String? emailError;
  String? passwordError;
  String? allError;

  LogInViewModel({AuthService? authService})
      : _authService = authService ?? AuthService();

  UserModel? get loggedInUser => _loggedInUser;
  bool get passwordVisible => _passwordVisible;
  bool get showValidationMessage => _showValidationMessage;
  bool get isLoading => _isLoading;
  String? get error => _error;

  void togglePasswordVisibility() {
    _passwordVisible = !_passwordVisible;
    notifyListeners();
  }

  String? validateTenancy(String? value, BuildContext context) {
    return (value == null || value.isEmpty)
        ? AppLocalizations.of(context)!.tenancyCodeRequired
        : null;
  }

  String? validateEmail(String? value, BuildContext context) {
    return (value == null || value.isEmpty)
        ? AppLocalizations.of(context)!.emailRequired
        : (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                .hasMatch(value))
            ? AppLocalizations.of(context)!.emailInvalid
            : null;
  }
/*
  String? validatePassword(String? value, BuildContext context) {
    return (value == null || value.isEmpty)
        ? AppLocalizations.of(context)!.passwordRequired
        : (!RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
                .hasMatch(value))
            ? AppLocalizations.of(context)!.passwordInvalid
            : null;
  }*/

//For testing authentication
  String? validatePassword(String? value, BuildContext context) {
    return (value == null || value.isEmpty)
        ? AppLocalizations.of(context)!.passwordRequired
        : null;
  }

  Future<bool> validateAll(/*String tenancyVal,*/ String emailVal,
      String passwordVal, BuildContext context) async {
    //tenancyCodeError = validateTenancy(tenancyVal, context);
    emailError = validateEmail(emailVal, context);
    passwordError = validatePassword(passwordVal, context);

    if (/*tenancyVal.isEmpty &&*/ emailVal.isEmpty && passwordVal.isEmpty) {
      allError = AppLocalizations.of(context)!.allRequired;
    } else {
      allError = null;
    }

    _showValidationMessage = tenancyCodeError != null ||
        emailError != null ||
        passwordError != null ||
        allError != null;

    notifyListeners();

    return !_showValidationMessage;
  }

  Future<Map<String, String>> getValidatedValues(
    /* String tenancyVal, */ String emailVal,
    String passwordVal,
    BuildContext context,
  ) async {
    bool isValid =
        await validateAll(/*tenancyVal,*/ emailVal, passwordVal, context);

    if (!isValid) {
      throw Exception("Validation failed");
    }

    return {
      //'tenancyCode': base64Encode(utf8.encode(tenancyVal)),
      'email': base64Encode(utf8.encode(emailVal)),
      'password': base64Encode(utf8.encode(passwordVal)),
    };
  }

  Future<void> login(
    BuildContext context,
    String email,
    String password,
    /*String tenancy*/
  ) async {
    _isLoading = true;
    _error = null;
    notifyListeners();

    try {
      final validatedValues = await getValidatedValues(
        email,
        password,
        context, /*, tenancy*/
      );
      if (validatedValues.isNotEmpty) {
        final user = await _authService.login(
            /*validatedValues['firstName']!,*/
            validatedValues['email']!,
            validatedValues['password']!);
        if (user != null) {
          _loggedInUser = user;
        } else {
          _error = 'Login failed';
        }
      }
    } catch (e) {
      _error = 'An error occurred: $e';
    }

    _isLoading = false;
    notifyListeners();
  }
}
