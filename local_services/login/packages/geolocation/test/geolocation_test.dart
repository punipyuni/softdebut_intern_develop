/*import 'package:flutter_test/flutter_test.dart';
import 'package:geolocation/geolocation_service.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

@GenerateMocks([GeolocatorPlatform])
void main() {
  group('GeolocationService', () {
    test('returns current location when location services are enabled', () async {
      final geolocationService = GeolocationService();
      final position = Position(
        latitude: 37.7749,
        longitude: -122.4194,
        timestamp: DateTime.now(),
        accuracy: 0,
        altitude: 0,
        heading: 0,
        speed: 0,
        speedAccuracy: 0,
      );
      
      final mockGeolocator = MockGeolocatorPlatform();
      when(mockGeolocator.isLocationServiceEnabled()).thenAnswer((_) async => true);
      when(mockGeolocator.checkPermission()).thenAnswer((_) async => LocationPermission.always);
      when(mockGeolocator.getCurrentPosition()).thenAnswer((_) async => position);

      GeolocatorPlatform.instance = mockGeolocator;

      final currentPosition = await geolocationService.getCurrentLocation();
      expect(currentPosition, isA<Position>());
      expect(currentPosition.latitude, 37.7749);
      expect(currentPosition.longitude, -122.4194);
    });
  });
}
*/