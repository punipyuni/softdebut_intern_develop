import 'package:flutter/material.dart';
import 'package:geolocation/geolocation_service.dart';

class LocationPermissionScreen extends StatefulWidget {
  @override
  _LocationPermissionScreenState createState() =>
      _LocationPermissionScreenState();
}

class _LocationPermissionScreenState extends State<LocationPermissionScreen> {
  final GeolocationService _geolocationService = GeolocationService();
  String _statusMessage = 'Requesting location permission...';

  @override
  void initState() {
    super.initState();
    _checkLocationPermission();
  }

  Future<void> _checkLocationPermission() async {
    try {
      await _geolocationService.getCurrentLocation();
      setState(() {
        _statusMessage = 'Location permission granted!';
      });
    } catch (e) {
      setState(() {
        _statusMessage = e.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Location Permission'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              _statusMessage,
              style: TextStyle(fontSize: 18),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: _checkLocationPermission,
              child: Text('Grant Permission'),
            ),
          ],
        ),
      ),
    );
  }
}
